#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import wx.lib.agw.aui as AUI

from centerpanel import CenterPanel

ID_SAVE_PERSPECTIVE = wx.ID_HIGHEST + 1
ID_LOAD_PERSPECTIVE = wx.ID_HIGHEST + 2

#===============================================================================
class MainFrame(wx.Frame):

    #---------------------------------------------------------------------------
    def __init__(self):
        """メインウィンドウの初期化"""
        wx.Frame.__init__(self, None, title=u'ぱいそんあっぷ', size=wx.Size(800, 600))

        self.buildPanes()
        self.buildMenuBar()
        self.buildEvents()
        self.CreateStatusBar()

    #---------------------------------------------------------------------------
    def buildPanes(self):
        """パネルの初期化"""
        self._mgr = AUI.AuiManager()
        self._mgr.SetAGWFlags(self._mgr.GetAGWFlags() ^ AUI.AUI_MGR_HINT_FADE)
        self._mgr.SetManagedWindow(self)
        self._mgr.GetArtProvider().GetFont(AUI.AUI_DOCKART_CAPTION_FONT).MakeBold()

        # ツールパネル
        pane = wx.Panel(self)
        tree = wx.TreeCtrl(pane, wx.ID_ANY, wx.Point(0, 0), wx.Size(160, 250),
                           wx.TR_DEFAULT_STYLE | wx.NO_BORDER)
        imglist = wx.ImageList(16, 16, True, 2)
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER, wx.ART_OTHER, wx.Size(16, 16)))
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, wx.Size(16, 16)))
        tree.AssignImageList(imglist)
        tree.AddRoot("root", 0)
        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(tree, 1, wx.EXPAND)
        pane.SetSizer(box)
        box.Fit(pane)
        self._mgr.AddPane(pane, AUI.AuiPaneInfo().Name('pane1').Caption(u"ぱね１").MinimizeButton(True))

        # ツールパネル
        pane = wx.Panel(self)
        tree = wx.TreeCtrl(pane, wx.ID_ANY, wx.Point(0, 0), wx.Size(160, 250),
                           wx.TR_DEFAULT_STYLE | wx.NO_BORDER)
        imglist = wx.ImageList(16, 16, True, 2)
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER, wx.ART_OTHER, wx.Size(16, 16)))
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, wx.Size(16, 16)))
        tree.AssignImageList(imglist)
        tree.AddRoot("root", 0)
        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(tree, 1, wx.EXPAND)
        pane.SetSizer(box)
        box.Fit(pane)
        self._mgr.AddPane(pane, AUI.AuiPaneInfo().Name('pane2').Caption(u"ぱね２").MinimizeButton(True).Hide())

        # センターパネル
        pane = CenterPanel(self)
        self._mgr.AddPane(pane, AUI.AuiPaneInfo().Name('center').CenterPane())

        self._mgr.Update()
        self._persp = self._mgr.SavePerspective()

        self._panes = {}
        for index, pane in enumerate(self._mgr.GetAllPanes()):
            if pane.IsToolbar():
                continue
            if not pane.caption or not pane.name:
                continue
            self._panes[wx.ID_HIGHEST + 0x1000 + index] = pane.window

    #---------------------------------------------------------------------------
    def buildMenuBar(self):
        """メニューバーの初期化"""
        mb = wx.MenuBar()

        file_menu = wx.Menu()
        file_menu.Append(wx.ID_EXIT, "Exit\tQ", u'あぷりを終了します')

        view_menu = wx.Menu()
        view_menu.Append(ID_SAVE_PERSPECTIVE, u"パースペクティブを退避")
        view_menu.Append(ID_LOAD_PERSPECTIVE, u"パースペクティブを復帰")
        view_menu.AppendSeparator()
        for ids, pane_name in self._panes.iteritems():
            pane = self._mgr.GetPane(pane_name)
            view_menu.AppendCheckItem(ids, pane.caption)

        mb.Append(file_menu, u"ファイル(&F)")
        mb.Append(view_menu, u"表示(&V)")

        self.SetMenuBar(mb)

    #---------------------------------------------------------------------------
    def buildEvents(self):
        """イベントハンドラの初期化"""

        self.Bind(wx.EVT_MENU, self.onMenuExit, id=wx.ID_EXIT)

        self.Bind(wx.EVT_MENU, self.onSavePerspective, id=ID_SAVE_PERSPECTIVE)
        self.Bind(wx.EVT_MENU, self.onLoadPerspective, id=ID_LOAD_PERSPECTIVE)
        for ids, pane_name in self._panes.iteritems():
            self.Bind(wx.EVT_MENU, self.onMenuPaneShow, id=ids)
            self.Bind(wx.EVT_UPDATE_UI, self.onUpdateUI, id=ids)

        self.Bind(wx.EVT_CLOSE, self.onClose)

    #---------------------------------------------------------------------------
    def onMenuExit(self, event = wx.CommandEvent):
        """終了イベント"""
        self.Close(True)

    #---------------------------------------------------------------------------
    def onSavePerspective(self, event = wx.CommandEvent):
        """パースペクティブの退避"""
        self._persp = self._mgr.SavePerspective()

    #---------------------------------------------------------------------------
    def onLoadPerspective(self, event = wx.CommandEvent):
        """パースぺくティブの復帰"""
        self._mgr.LoadPerspective(self._persp, True)

    #---------------------------------------------------------------------------
    def onMenuPaneShow(self, event = wx.CommandEvent):
        """パネルを開く"""
        id_target = event.GetId()
        for ids, pane_window in self._panes.iteritems():
            if ids == id_target:
                pane = self._mgr.GetPane(pane_window)
                assert pane.IsShown() == False
                pane.Left().Show()
                self._mgr.Update()
                break
            
    #---------------------------------------------------------------------------
    def onUpdateUI(self, event = wx.UpdateUIEvent):
        """メニューのチェックボックス更新とか"""
        id_target = event.GetId()
        for ids, pane_window in self._panes.iteritems():
            if ids == id_target:
                pane = self._mgr.GetPane(pane_window)
                shown = pane.IsShown()
                event.Check(shown)
                event.Enable(not shown)
                break

    #---------------------------------------------------------------------------
    def onClose(self, event = wx.CloseEvent):
        """ウィンドウを閉じる"""
        self._mgr.UnInit()
        event.Skip()
