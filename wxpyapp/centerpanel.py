#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import wx.lib.agw.fourwaysplitter as FWS

#===============================================================================
class CenterPanel(wx.Panel):
    """センターパネル"""

    #---------------------------------------------------------------------------
    def __init__(self, parent):
        """初期化"""
        wx.Panel.__init__(self, parent)

        splitter = FWS.FourWaySplitter(self)
        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(splitter, 1, wx.EXPAND)
        self.SetSizer(box)

        self._buttons = {}
        for x in xrange(4):
            pane = wx.Panel(splitter, style=wx.BORDER_SUNKEN)
            button = wx.ToggleButton(pane, label="hoge %d" % x)
            self.Bind(wx.EVT_TOGGLEBUTTON, self.onToggle, button)
            splitter.AppendWindow(pane)
            self._buttons[button.GetId()] = x

        self._splitter = splitter

        # self.Bind(wx.EVT_SIZE, self.onSize)
        #self.Bind(wx.EVT_ERASE_BACKGROUND, self.onEraseBackground)
        #self.Bind(wx.EVT_PAINT, self.onPaint)

    #---------------------------------------------------------------------------
    def onToggle(self, event = wx.CommandEvent):
        """トグルボタン"""
        if event.IsChecked():
            ids = self._buttons[event.GetId()]
            self._splitter.SetExpanded(ids)
        else:
            self._splitter.SetExpanded(-1)

    # #---------------------------------------------------------------------------
    # def onSize(self, event = wx.SizeEvent):
    #     """ウィンドウサイズ変更イベント"""
    #     print 'size:', event.Size
    #     event.Skip()

    # #---------------------------------------------------------------------------
    # def onEraseBackground(self, event = wx.EraseEvent):
    #     """背景消し"""
    #     pass

    # #---------------------------------------------------------------------------
    # def onPaint(self, event = wx.PaintEvent):
    #     """センターパネルの描画処理"""
    #     #dc = wx.PaintDC(self)
    #     #dc.Clear()
    #     #dc.DrawLine(10, 10, 100, 100)
