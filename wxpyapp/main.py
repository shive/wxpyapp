#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import sys
import wx

from mainframe import MainFrame

#===============================================================================
def main():
    """main関数"""

    app = wx.App()
    frame = MainFrame()
    frame.Show()
    app.MainLoop()

    return 0

#===============================================================================
if __name__ == '__main__':
    main()
